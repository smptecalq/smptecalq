#!/bin/sh
# installer for smpte-calq
ARG="$1"

if [ "X$ARG" = "X" ]
    then echo "Sytax Error: You must tell me whether you want to install this just for yourself, or for all users."
    echo "install.sh --system"
    echo "install.sh --user"
    exit 1
fi

if [ "X$ARG" = "X--system" ]
    then
    ID=`id -u`
    if [ "x$ID" != "x0" ]
    then 
	echo "System Error: You must run this as root!"
	ecjp "Example: sudo ./install.sh --system"
	echo "Note: If you do not have root priviledges, you can still install this locally with ./install.sh --user"
        exit 1
	else
	OP="/usr/local/bin"
    fi
else
    OP="/home/$USER/bin"
fi

## find out what system we are running

KERN=$(uname -av | grep -i darwin | cut -f1 -d" ")
if [ "X$KERN" = "XDarwin" ]
    then
    APP="./smptecalq.app"
    OP="/Applications"
    else
    APP="./smptecalq.pyw"
fi

# copy the goods to the right place.

cp -rf $APP $OP/smptecalq
OK=$(ls $OP | grep smptecalq | cut -f1 -d" ")
if [ "X$OK" = "X" ]
	then 
	echo "Installation encountered some errors.  This might not have worked.  Perhaps try manually installing."
	exit 1
    else echo "Installation Complete"
fi
